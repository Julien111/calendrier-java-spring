package com.calendrier.projetun.util;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@Data
@FieldDefaults(level=AccessLevel.PRIVATE)
public class NbInscrits {

	int annee;
	int mois;
	long nbInscrits;
	
}