package com.calendrier.projetun.exception;

public class JourExistantException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JourExistantException() {
		super();		
	}

	public JourExistantException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public JourExistantException(String message, Throwable cause) {
		super(message, cause);		
	}
	
	/**
	 * Toutes les exceptions peuvent porter un message. Ce message on le donne en paramètre au constructeur.
	 * @param message
	 */
	public JourExistantException(String message) {
		super(message);		
	}

	public JourExistantException(Throwable cause) {
		super(cause);		
	}	

}
