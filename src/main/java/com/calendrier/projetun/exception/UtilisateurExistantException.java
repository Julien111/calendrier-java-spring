package com.calendrier.projetun.exception;

public class UtilisateurExistantException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UtilisateurExistantException() {
		super();		
	}

	public UtilisateurExistantException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);		
	}

	public UtilisateurExistantException(String message, Throwable cause) {
		super(message, cause);		
	}

	public UtilisateurExistantException(String message) {
		super(message);		
	}

	public UtilisateurExistantException(Throwable cause) {
		super(cause);		
	}
	
	

}
