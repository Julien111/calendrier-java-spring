package com.calendrier.projetun.exception;

public class ThemeExistantException extends RuntimeException {

	/**
	 * serialVersion
	 */
	private static final long serialVersionUID = 1L;

	public ThemeExistantException() {
		super();		
	}

	public ThemeExistantException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);		
	}

	public ThemeExistantException(String message, Throwable cause) {
		super(message, cause);		
	}

	public ThemeExistantException(String message) {
		super(message);		
	}

	public ThemeExistantException(Throwable cause) {
		super(cause);		
	}
	
	

}
