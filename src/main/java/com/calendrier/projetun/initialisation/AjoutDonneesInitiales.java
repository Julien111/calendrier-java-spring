package com.calendrier.projetun.initialisation;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.calendrier.projetun.business.Emotion;
import com.calendrier.projetun.business.Jour;
import com.calendrier.projetun.business.Theme;
import com.calendrier.projetun.business.Utilisateur;
import com.calendrier.projetun.dao.EmotionDao;
import com.calendrier.projetun.dao.JourDao;
import com.calendrier.projetun.dao.ThemeDao;
import com.calendrier.projetun.dao.UtilisateurDao;

import lombok.AllArgsConstructor;


@Component
@AllArgsConstructor
public class AjoutDonneesInitiales implements CommandLineRunner {
	
    private final EmotionDao emotionDao;
    
    private final ThemeDao themeDao;
    
    private final JourDao jourDao;
    
    private final UtilisateurDao utilisateurDao;
	

	@Override
    @Transactional
    public void run(String... args) throws Exception {
       Date dateHeureDebut = new Date();
       ajouterEmotions();
       ajouterThemes();
       ajouterJours();
       Date dateHeureFin = new Date();
       System.out.println("Données initiales ajoutées en "
             + String.valueOf(dateHeureFin.getTime() - dateHeureDebut.getTime()) + " ms");
       ajouterUtilisateur();
    }
    
    private void ajouterEmotions() {
        if (emotionDao.count() == 0) { 
            emotionDao.save(new Emotion("Souriant", "&#x1F600;"));
            emotionDao.save(new Emotion("Monocle", "&#x1F9D0;"));
            emotionDao.save(new Emotion("Bisous", "&#x1F618;"));
            emotionDao.save(new Emotion("Coeur", "&#x1F60D;"));
            emotionDao.save(new Emotion("PTDR", "&#x1F923;"));
        }
    }   
    
    private void ajouterThemes() {
        if (themeDao.count() == 0) {
            themeDao.save(new Theme("Bachata"));
            themeDao.save(new Theme("Dark"));
        }
    }
    
    private void ajouterJours() {
        if (jourDao.count() == 0) {
            int anneeEnCours = LocalDate.now().getYear();
            int moisEnCours = LocalDate.now().getMonthValue();
            LocalDate date = LocalDate.of(anneeEnCours, moisEnCours, 1);
            int nbJoursDuMoisEnCours = date.lengthOfMonth();
            for (int i = 1; i <= nbJoursDuMoisEnCours; i++) {
                jourDao.save(new Jour(date));
                date = date.plusDays(1);
            }
        }
    }
    
 private void ajouterUtilisateur() {
	 
	 LocalDateTime date1 = LocalDateTime.now();
	 
	 Theme theme = themeDao.findByNom("Dark");
	 Theme theme2 = themeDao.findByNom("Bachata");
    	
    	if(utilisateurDao.count() == 0) {
    		utilisateurDao.save(new Utilisateur("Marcus", "Fred", "fred@orsys.fr", "12345678",  date1, theme ));
    		utilisateurDao.save(new Utilisateur("Gregoire", "John", "greg@orsys.fr", "87654321",  date1, theme2 ));
    	}
       
    }
}
