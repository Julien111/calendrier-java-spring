package com.calendrier.projetun.service;

import java.util.List;

import com.calendrier.projetun.business.Emotion;

public interface EmotionService {

	public Emotion getEmotionById(Long id);

	public List<Emotion> getEmotions();

	public Emotion createEmotion(Emotion emotion);
	
	public Emotion createEmotion(String nom, String code);

	public Emotion updateEmotion(Emotion emotion);

	public boolean deleteEmotion(Long id);
	
	public Emotion retrieveEmotion(String nom);

	public boolean existsById(long id);
}
