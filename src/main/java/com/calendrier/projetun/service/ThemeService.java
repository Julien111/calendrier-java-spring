package com.calendrier.projetun.service;

import java.util.List;

import com.calendrier.projetun.business.Theme;

public interface ThemeService {
	
	public Theme getThemeById(Long id);
	
	public List<Theme> getThemes();
	
	public Theme createTheme(Theme theme);
	
	public Theme updateTheme(Theme theme);
	
	public void deleteTheme(Theme theme);
	
	public boolean existsById(long id);	
	
	public Theme createTheme(String nom); 

    public Theme recupererTheme(String nom);
}
