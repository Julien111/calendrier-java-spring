package com.calendrier.projetun.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calendrier.projetun.business.Theme;
import com.calendrier.projetun.dao.ThemeDao;
import com.calendrier.projetun.exception.ThemeExistantException;
import com.calendrier.projetun.service.ThemeService;

@Service
public class ThemeServiceImpl implements ThemeService {
	
	@Autowired
	private ThemeDao themeDao;

	public ThemeServiceImpl(ThemeDao themeDao) {
		super();
		this.themeDao = themeDao;
	}

	@Override
	public Theme getThemeById(Long id) {
		Theme theme = themeDao.findById(id).orElse(null);
		if(theme == null) {
			throw new ThemeExistantException("Le thême est null.");
		}
	    return theme;
	}

	@Override
	public List<Theme> getThemes() {
		return themeDao.findAll();
	}

	@Override
	public Theme createTheme(Theme theme) {
		
		List<Theme> listeThemes =  getThemes();
		
		for (Theme element : listeThemes) {
			if(element.getNom().equals(theme.getNom())) {
				throw new ThemeExistantException("Le thême existe déjà.");
			}
		}		
		themeDao.save(theme);
		return theme;
	}

	@Override
	public Theme updateTheme(Theme theme) {
		return themeDao.save(theme);	
	}

	@Override
	public void deleteTheme(Theme theme) {		
		themeDao.delete(theme);
	}
	
	@Override
	public boolean existsById(long id){
        return themeDao.existsById(id);
    }

	@Override
	public Theme createTheme(String nom) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Theme recupererTheme(String nom) {
		//on invoque une méthode qui utilise une requête par dérivation
		return themeDao.findByNom(nom);
	}
	
}
