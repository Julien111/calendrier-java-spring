package com.calendrier.projetun.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calendrier.projetun.business.Gif;
import com.calendrier.projetun.dao.GifDao;
import com.calendrier.projetun.service.GifService;

@Service
public class GifTeleverseServiceImpl implements GifService {
	
	@Autowired
	private GifDao gifDao;
	
	public GifTeleverseServiceImpl() {
		super();
	}
	
	@Override
	public List<Gif> getGifs() {
		return gifDao.findAll();
	}

	@Override
	public Gif createGif(Gif gif) {
		return gifDao.save(gif);
	}

	@Override
	public Gif updateGif(Gif gif) {
		return gifDao.save(gif);
	}

	@Override
	public void deleteGif(Gif gif) {
		gifDao.delete(gif);				
	}

	@Override
	public boolean existGif(Long id) {
		return gifDao.existsById(id);
	}

	@Override
	public Gif getGifById(Long id) {
		return gifDao.findById(id).orElse(null);
	}
	
}
