package com.calendrier.projetun.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calendrier.projetun.business.Jour;
import com.calendrier.projetun.dao.JourDao;
import com.calendrier.projetun.dto.JourDto;
import com.calendrier.projetun.exception.JourExistantException;
import com.calendrier.projetun.mapper.JourMapper;
import com.calendrier.projetun.mapper.JourMapperImpl;
import com.calendrier.projetun.service.JourService;

@Service
public class JourServiceImpl implements JourService {
	
	@Autowired
	JourDao jourDao;
	
	JourMapper map = new JourMapperImpl();

	public JourServiceImpl() {
		super();		
	}	

	@Override
	public List<Jour> getJours() {
		return jourDao.findAll();
	}

	@Override
	public Jour createJour(Jour jour) {		
		if (jourDao.existsById(jour.getDate())) {
            throw new JourExistantException("Le jour est déjà en base");
        }		
		return jourDao.save(jour);
	}
	
	/**
	 * méthode avec dto 
	 * plus sécurisée
	 */
	@Override
	 public Jour postJour(JourDto jourDto) {				
			
		if (jourDao.existsById(jourDto.getDate())) {
            throw new JourExistantException("Le jour est déjà en base");
        }			
		Jour newJour = map.toEntity(jourDto);
		return jourDao.save(newJour);		 
	 }
	
	@Override
	public Jour createJour(LocalDate date) {
		Jour jour = new Jour();
		jour.setDate(date);
		if (jourDao.existsById(jour.getDate())) {
            throw new JourExistantException("Le jour est déjà en base");
        }		
		return jourDao.save(jour);
	}

	@Override
	public Jour updateJour(Jour jour) {		
		return jourDao.save(jour);
	}
	
	@Override
	public boolean deleteJour(LocalDate date) {		
		if (jourDao.existsById(date)) {
			jourDao.deleteById(date);		
			return true;
		}
		return false;
	}
	
	public Jour updateDayAtDay(LocalDate date, int nouveauNbPoints) {
		Jour jour = jourDao.findById(date).orElse(null);
		if (jour == null) {
            throw new JourExistantException("Le jour est null");
        }	
		jour.setNbPoints(nouveauNbPoints);
		return jourDao.save(jour);
	}
		
	public List<Jour> findByGifIsNull () {
		return jourDao.findByGifIsNull();
	}
	
	public List<Jour> findDaysOfCurrentMonth(){
		return jourDao.findDaysOfCurrentMonth();
	}
	
	public Integer findAverageOfPointsInSeptember2022() {
		return jourDao.findAverageOfPointsInSeptember2022();
	}

}
