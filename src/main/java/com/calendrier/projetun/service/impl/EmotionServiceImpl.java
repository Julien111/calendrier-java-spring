package com.calendrier.projetun.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.calendrier.projetun.business.Emotion;
import com.calendrier.projetun.dao.EmotionDao;
import com.calendrier.projetun.service.EmotionService;

@Service
public class EmotionServiceImpl implements EmotionService {
	
	private EmotionDao emotionDao;
	
	public EmotionServiceImpl(EmotionDao emotionDao) {		
		super();
		this.emotionDao = emotionDao;
	}

	@Override
	public Emotion getEmotionById(Long id) {
		return emotionDao.findById(id).orElse(null);
	}

	@Override
	public List<Emotion> getEmotions() {
				return emotionDao.findAll();
	}
	
	@Override
	public Emotion retrieveEmotion(String nom) {
		return emotionDao.findByNom(nom);
	}

	@Override
	public Emotion createEmotion(Emotion emotion) {
		return emotionDao.save(emotion);		
	}
	
	@Override
	public Emotion createEmotion(String nom, String code) {
		return emotionDao.save(new Emotion(nom, code));		
	}

	@Override
	public Emotion updateEmotion(Emotion emotion) {		
		return emotionDao.save(emotion);	
	}

	@Override
	public boolean deleteEmotion(Long id) {
		  Emotion emotion = getEmotionById(id);
	        if (emotion==null) {
	            return false;
	        }
	        else {
	            emotionDao.delete(emotion);
	            return true;
	        }
	}	
	
	@Override
	public boolean existsById(long id) {
		return emotionDao.existsById(id);
	}

}
