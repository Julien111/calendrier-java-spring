package com.calendrier.projetun.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.calendrier.projetun.business.Gif;
import com.calendrier.projetun.dao.GifDao;

public abstract class GifServiceImpl {
	
	@Autowired
	protected GifDao gifDao;

	protected GifServiceImpl()  {
		super();
	}
	
	protected abstract Gif getGifById(Long id) ;	
	
	protected abstract List<Gif> getGifs();	
	
	protected abstract Gif createGif(Gif gif);
	
	protected abstract Gif updateGig(Gif gif);
	
	protected abstract  void deleteGif(Gif gif);
	
	protected abstract  boolean existGif(Long id);
}
