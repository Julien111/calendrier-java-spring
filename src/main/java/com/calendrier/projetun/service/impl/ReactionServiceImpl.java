package com.calendrier.projetun.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calendrier.projetun.business.Reaction;
import com.calendrier.projetun.dao.ReactionDao;
import com.calendrier.projetun.service.ReactionService;

@Service
public class ReactionServiceImpl implements ReactionService {
	
	@Autowired
	private ReactionDao reactionDao;

	public ReactionServiceImpl() {
		super();
	}	

	public ReactionServiceImpl(ReactionDao reactionDao) {
		super();
		this.reactionDao = reactionDao;
	}

	@Override
	public Reaction getReactionById(Long id) {
		return reactionDao.findById(id).orElse(null);
	}

	@Override
	public List<Reaction> getReactions() {
		return reactionDao.findAll();
	}

	@Override
	public Reaction creactionReaction(Reaction reaction) {
		reactionDao.save(reaction);
		return reaction;
	}

	@Override
	public Reaction updateReaction(Reaction reaction) {
		return reactionDao.save(reaction);	
	}

	@Override
	public void deleteReaction(Long id) {		
		reactionDao.deleteById(id);
	}	

}
