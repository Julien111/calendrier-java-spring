package com.calendrier.projetun.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calendrier.projetun.business.Jour;
import com.calendrier.projetun.business.Theme;
import com.calendrier.projetun.business.Utilisateur;
import com.calendrier.projetun.dao.UtilisateurDao;
import com.calendrier.projetun.dto.JourDto;
import com.calendrier.projetun.dto.UtilisateurDto;
import com.calendrier.projetun.exception.JourExistantException;
import com.calendrier.projetun.exception.UtilisateurExistantException;
import com.calendrier.projetun.exception.UtilisateurNonTrouveException;
import com.calendrier.projetun.service.ThemeService;
import com.calendrier.projetun.service.UtilisateurService;
import com.calendrier.projetun.util.NbInscrits;

@Service
@Transactional
public class UtilisateurServiceImpl implements UtilisateurService {	
	
	@Autowired
	private final UtilisateurDao utilisateurDao;
	private final ThemeService themeService;

	public UtilisateurServiceImpl(UtilisateurDao utilisateurDao, ThemeService themeService) {
		super();		
		this.utilisateurDao = utilisateurDao;
		this.themeService = themeService;
	}

	@Override
	public Utilisateur getUserById(Long id) {		
		return utilisateurDao.findById(id).orElse(null);		
	}

	@Override
	public List<Utilisateur> getUsers() {
		return utilisateurDao.findAll();		
	}

	@Override
	public Utilisateur createUser(Utilisateur utilisateur) {
		System.out.println("utilisateur.getId()=" + utilisateur.getId());

        if (utilisateur.getId()==null && utilisateurDao.existsByEmail(utilisateur.getEmail())) {
                throw new UtilisateurExistantException("L'adresse email est déjà présente en base");
        }

        return utilisateurDao.save(utilisateur);		
	}
	
	/**
	 * méthode avec dto 
	 * plus sécurisée
	 */
	@Override
	 public Utilisateur postUtilisateur(UtilisateurDto userDto) {				
		Utilisateur utilisateur =  new Utilisateur();
		
		if (utilisateurDao.existsByEmail(userDto.getEmail())) {
            throw new JourExistantException("L'email est déjà en base");
        }			
		utilisateur.setEmail(userDto.getEmail());
		utilisateur.setNom(userDto.getNom());
		utilisateur.setPrenom(userDto.getPrenom());
		utilisateur.setMotDePasse(userDto.getMotDePasse());
		utilisateur.setDateHeureInscription(userDto.getDateHeureInscription());		
		utilisateur.setTheme(userDto.getTheme());		
		
		return utilisateurDao.save(utilisateur);		
	 }

	@Override
	public Utilisateur updateUser(Utilisateur utilisateur) {
		return utilisateurDao.save(utilisateur);		
	}

	@Override
	public void deleteUser(Utilisateur user) {		
		utilisateurDao.delete(user);
	}

	@Override
	public Utilisateur createUser(String nom, String prenom, String email, String motDePasse, Theme theme) {		
		Utilisateur utilisateur = new Utilisateur();
        utilisateur.setNom(nom);
        utilisateur.setPrenom(prenom);
        utilisateur.setEmail(email);
        utilisateur.setMotDePasse(motDePasse);
        utilisateur.setTheme(theme);
        utilisateur.setNbPoints(500);
        LocalDateTime dateInscription = LocalDateTime.now();
        utilisateur.setDateHeureInscription(dateInscription);
        return utilisateurDao.save(utilisateur);
	}

	@Override
	public Utilisateur recupererUtilisateur(String email, String motDePasse) {	
		List<Utilisateur> liste = getUsers();
		Utilisateur utilisateur = null;
		for(Utilisateur user : liste) {
			if(user.getEmail().equals(email) && user.getMotDePasse().equals(motDePasse)) {
				utilisateur = user;
			}
		}		
		if(utilisateur == null) {
			throw new UtilisateurNonTrouveException("Vos identifiants sont incorrects");
		}
		return utilisateur;		
	}

	@Override
	public List<Utilisateur> recupererUtilisateursAyantReagiAuMoinsCinqFois() {		
		List<Utilisateur> liste = getUsers();
		List<Utilisateur>  retour = new ArrayList<>();
		
		for(Utilisateur user : liste) {
			if(user.getGifs().size() > 5) {
				retour.add(user);
			}
		}		
		return retour;
	}

	@Override
	public Utilisateur ajouterUtilisateurAleatoire() {		
		return null;
	}

	@Override
	public List<NbInscrits> recupererNbInscrits() {		
		return null;
	}
	
		

}
