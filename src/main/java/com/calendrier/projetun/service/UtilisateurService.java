package com.calendrier.projetun.service;

import java.util.List;

import com.calendrier.projetun.business.Theme;
import com.calendrier.projetun.business.Utilisateur;
import com.calendrier.projetun.dto.UtilisateurDto;
import com.calendrier.projetun.util.NbInscrits;

public interface UtilisateurService {
	
	public Utilisateur getUserById(Long id);
	
	public List<Utilisateur> getUsers();
	
	public Utilisateur createUser(Utilisateur utilisateur);
	
	 public Utilisateur postUtilisateur(UtilisateurDto userDto);
	
	 public Utilisateur createUser(String nom, String prenom, String email, String motDePasse, Theme recupererTheme);
	
	public Utilisateur updateUser(Utilisateur utilisateur);
	
	public void deleteUser(Utilisateur user);  

    public Utilisateur recupererUtilisateur(String email, String motDePasse);

    public List<Utilisateur> recupererUtilisateursAyantReagiAuMoinsCinqFois();
    
    public Utilisateur ajouterUtilisateurAleatoire();

    public List <NbInscrits> recupererNbInscrits();
    
}
