package com.calendrier.projetun.service;

import java.util.List;

import com.calendrier.projetun.business.Reaction;

public interface ReactionService {
	
	public Reaction getReactionById(Long id);
	
	public List<Reaction> getReactions() ;
	
	public Reaction creactionReaction(Reaction reaction);
	
	public Reaction updateReaction(Reaction reaction);	
	
	public void deleteReaction(Long id);
		
}
