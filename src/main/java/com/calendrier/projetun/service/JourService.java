package com.calendrier.projetun.service;

import java.time.LocalDate;
import java.util.List;

import com.calendrier.projetun.business.Jour;
import com.calendrier.projetun.dto.JourDto;

public interface JourService {
	
	public List<Jour> getJours();
	
	public Jour createJour(Jour jour);
	
	 public Jour postJour( JourDto jourDto);
	
	public Jour createJour(LocalDate date);
	
	public Jour updateJour(Jour jour);
	
	public Jour updateDayAtDay(LocalDate date, int nouveauNbPoints);
	
	public boolean deleteJour(LocalDate date);
}
