package com.calendrier.projetun.service;

import java.util.List;

import com.calendrier.projetun.business.Gif;

public interface GifService {
	
	public Gif getGifById(Long id);
	
	public List<Gif> getGifs();
	
	public Gif createGif(Gif gif);
	
	public Gif updateGif(Gif gif);
	
	public void deleteGif(Gif gif);
	
	public  boolean existGif(Long id);
}
