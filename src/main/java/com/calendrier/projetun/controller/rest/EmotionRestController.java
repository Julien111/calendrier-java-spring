package com.calendrier.projetun.controller.rest;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.calendrier.projetun.business.Emotion;
import com.calendrier.projetun.service.EmotionService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class EmotionRestController {
	
	private EmotionService emotionService;
	
    @GetMapping("/emotions")
    public List<Emotion> getEmotions() {
        return emotionService.getEmotions();
    }
    
    // Dans l'URL on attend 2 informations
    @PostMapping("/emotions")
     public Emotion postEmotion(String nom, String code) {
        return emotionService.createEmotion(nom, code);
    }
    
    @DeleteMapping(value = "/emotions/{id}")
    public boolean deleteEmotion(@PathVariable("id") Long  id) {
        return emotionService.deleteEmotion(id);
    }

}
