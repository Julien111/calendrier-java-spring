package com.calendrier.projetun.controller.rest;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.calendrier.projetun.business.Theme;
import com.calendrier.projetun.business.Utilisateur;
import com.calendrier.projetun.dto.UtilisateurDto;
import com.calendrier.projetun.exception.UtilisateurExistantException;
import com.calendrier.projetun.exception.UtilisateurNonTrouveException;
import com.calendrier.projetun.service.ThemeService;
import com.calendrier.projetun.service.UtilisateurService;

@RestController
public class UtilisateurRestController {

	private final UtilisateurService utilisateurService;
	private final ThemeService themeService;

	public UtilisateurRestController(UtilisateurService utilisateurService, ThemeService themeService) {
		this.utilisateurService = utilisateurService;
		this.themeService = themeService;
	}

	@GetMapping("/utilisateurs")
	public List<Utilisateur> getall() {
		return utilisateurService.getUsers();
	}

	@PostMapping("/utilisateurs")
	public Utilisateur createUser(@RequestBody Utilisateur utilisateur) {
		return utilisateurService.createUser(utilisateur);
	}
	
	/**
	 * 3 façon d'envoyer des informations à l'api Rest
	 * request body
	 * variable de chemin
	 * request param
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param motDePasse
	 * @param theme
	 * @return
	 */
	@PostMapping("/utilisateurs/fields")
	public Utilisateur createUser(@RequestBody Map<String, String> map){		
		Theme theme1 = themeService.recupererTheme(map.get("nomTheme"));	
		return utilisateurService.createUser(map.get("nom"), map.get("prenom"),  map.get("email"), map.get("motDePasse"), theme1);
	}
	
	@PostMapping("/utilisateurs/dto/create")
	public Utilisateur createUserDto(@RequestBody Map<String, String> map){		
		Theme theme1 = themeService.recupererTheme(map.get("nomTheme"));	
		LocalDateTime date = LocalDateTime.now();
		UtilisateurDto user = new UtilisateurDto(map.get("nom"), map.get("prenom"),  map.get("email"), map.get("motDePasse"), date,   theme1);
		
		return utilisateurService.postUtilisateur(user);
	}

	@PostMapping("/utilisateur/recuperer")
	public Utilisateur retrieveUser(@RequestBody String email, @RequestBody String password) {
		return utilisateurService.recupererUtilisateur(email, password);
	}
	
//	@PatchMapping("/utilisateurs/modifier")
//	public Utilisateur updateUser(@RequestBody Long id, @RequestBody String nom, @RequestBody String prenom, @RequestBody  String email, @RequestBody String password) {
//		
//	}

	@DeleteMapping(value = "/utilisateurs/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public void deleteUtilisateur(@PathVariable("id") Long id) {
		Utilisateur user = utilisateurService.getUserById(id);
		utilisateurService.deleteUser(user);
	}

	@ExceptionHandler(UtilisateurExistantException.class)
	@ResponseStatus(code = HttpStatus.CONFLICT)
	public String userDejaEnBase(Exception e) {
		return e.getMessage();
	}

	@ExceptionHandler(UtilisateurNonTrouveException.class)
	@ResponseStatus(code = HttpStatus.CONFLICT)
	public String userNonTrouve(Exception e) {
		return e.getMessage();
	}

	@ExceptionHandler(javax.validation.ConstraintViolationException.class)
	@ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
	public List<String> traiterDonneesInvalidesAvecDetails(ConstraintViolationException exception) {
		return exception.getConstraintViolations().stream().map(ConstraintViolation::getMessage)
				.collect(Collectors.toList());
	}

}
