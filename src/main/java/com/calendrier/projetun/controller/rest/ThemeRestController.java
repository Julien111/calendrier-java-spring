package com.calendrier.projetun.controller.rest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.calendrier.projetun.business.Theme;
import com.calendrier.projetun.exception.ThemeExistantException;
import com.calendrier.projetun.service.ThemeService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class ThemeRestController {

	private ThemeService themeService;

	@GetMapping("/themes")
	public List<Theme> getThemes() {
		return themeService.getThemes();
	}
	
	@PostMapping("/themes")
    @ResponseStatus(HttpStatus.CREATED)
	public void createTheme(String nom) {
		Theme theme = new Theme();
		theme.setNom(nom);
		themeService.createTheme(theme);
	}
	
	@DeleteMapping(value = "/themes/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public void deleteTheme(@PathVariable("id") Long id) {
		Theme theme = themeService.getThemeById(id);		
		themeService.deleteTheme(theme);		
	}

	@ExceptionHandler(ThemeExistantException.class)
	@ResponseStatus(code = HttpStatus.CONFLICT)
	public String traiterThemeDejaEnBase(Exception e) {
		return e.getMessage();
	}

}
