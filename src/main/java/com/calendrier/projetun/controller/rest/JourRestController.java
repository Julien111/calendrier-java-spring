package com.calendrier.projetun.controller.rest;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.calendrier.projetun.business.Jour;
import com.calendrier.projetun.dto.JourDto;
import com.calendrier.projetun.exception.JourExistantException;
import com.calendrier.projetun.service.JourService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
public class JourRestController {
	
private JourService jourService;
	
    @GetMapping("/jours")
    public List<Jour> getEmotions() {
        return jourService.getJours();
    }
    
    /**
     * Ajoute un jour qui a été placé dans le corps de la requête http
     * @param jour
     * @return
     */
    @PostMapping("jours")
    @ResponseStatus(HttpStatus.CREATED)
    public Jour postJour(@RequestBody Jour jour) {
    	return jourService.createJour(jour);
    }   
    
    /**
     * Ajoute un jour qui a été placé dans le corps de la requête http
     * @param jourDto
     * @return
     */
    @PostMapping("jours/dto")
    @ResponseStatus(code=HttpStatus.CREATED)
    @Operation(description="Ajoute un jour en base")
    public Jour postJour(@RequestBody JourDto jourDto
            ) {
        return jourService.postJour(jourDto);
        
    }
    
    @PostMapping("jours/{date}")
    public Jour postJour(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PathVariable @Parameter(description="date yyyy-MM-dd") LocalDate date) {
    	return jourService.createJour(date);
    }   
   
    @DeleteMapping(value = "/jours/{date}")
    public boolean deleteJour(@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) @PathVariable("date") @Parameter(description="date yyyy-MM-dd") LocalDate date) {
        return jourService.deleteJour(date);
    }
    
    @PatchMapping("jours/{date}/{nouveauNbPoints}")
    public Jour patchJour(@PathVariable @DateTimeFormat(iso=ISO.DATE) LocalDate date, @PathVariable int nouveauNbPoints) {
        return jourService.updateDayAtDay(date, nouveauNbPoints);
    }
    
    @ExceptionHandler(JourExistantException.class)
    @ResponseStatus(code=HttpStatus.CONFLICT)
    public String traiterJourDejaEnBase(Exception e) {
    	return e.getMessage();
    }
    
    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    @ResponseStatus(code=HttpStatus.UNPROCESSABLE_ENTITY)
    public List<String> traiterDonneesInvalidesAvecDetails(ConstraintViolationException exception) {
        return exception.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.toList());
    }
}
