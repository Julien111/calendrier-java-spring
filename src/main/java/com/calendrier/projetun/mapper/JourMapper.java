package com.calendrier.projetun.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.calendrier.projetun.business.Jour;
import com.calendrier.projetun.dto.JourDto;

@Mapper(componentModel = "spring")
public interface JourMapper {
	JourMapper INSTANCE = Mappers.getMapper(JourMapper.class);

    JourDto toDto(Jour jour);

    Jour toEntity(JourDto jourDto);
}
