package com.calendrier.projetun.business;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.PositiveOrZero;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@EqualsAndHashCode
@Entity
@Table(name="jour")
public class Jour {
	
	private static final int NB_POINTS_PAR_DEFAUT = 50;
	
	@Id	
    @Column(name = "date")
	@NonNull
	private LocalDate date;
	
	@PositiveOrZero(message="Le chiffre est infèrieure à zéro.")
	@Column(name="nb_points", nullable = true)
	@Range(min=20, max=50, message="Le nombre de points doit être compris entre {min} et {max}.")
	private int nbPoints = 30;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
	@JoinColumn(name="id_gif")
	@JsonIgnore
	@ToString.Exclude
    private Gif gif;

	public Jour() {
		super();		
	}	
	
	public Jour(LocalDate date) {
        this(date, NB_POINTS_PAR_DEFAUT);
    }

    public Jour(LocalDate date, int nbPoints) {
        this.date = date;
        this.nbPoints = nbPoints;
    }

    public String toString() {
        return date.getDayOfMonth() + "/" +  date.getMonthValue();
    }
    
	
}
