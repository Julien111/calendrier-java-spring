package com.calendrier.projetun.business;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@RequiredArgsConstructor
@Entity
@Table(name="utilisateur")
public class Utilisateur {	
	
	@Column(name = "nb_points_initial")
	@Transient
	private final int NB_POINTS_INITIAL = 500;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long id;
	
	@Column(name = "nom")
	@NotBlank
	@NonNull
	private String nom;
	
	@Column(name = "prenom")
	@NotBlank
	@NonNull
	private String prenom;
	
	@Email(message="L'adresse email n'est pas valide !")
	@Column(name = "email", unique=true)
	@NotBlank(message="Merci de préciser un email !")
	@NonNull
	@Pattern(regexp="^([A-Za-z0-9-])+(.[A-Za-z0-9-]+)*@orsys.fr$", message="Votre adresse doit se terminer par @orsys.fr")
	private String email;
	
	@Column(name = "mot_de_passe")
	@NotBlank
	@NonNull
	@Length(min=8, max=200, message="Le mot de passe doit être compris entre 8 et 200 caractères")
	private String motDePasse;
	
	@Column(name = "nb_points")			
	private int nbPoints = NB_POINTS_INITIAL;
		
    @Column(name = "date_heure_inscription")
    @NonNull    
	private LocalDateTime dateHeureInscription;
	
	@OneToMany(mappedBy="utilisateur", fetch=FetchType.EAGER)
	@JsonIgnore
	private List<Gif> gifs;
	
	@ManyToOne
	@JoinColumn(name="id_theme")
	@NonNull
	private Theme theme;	

	public Utilisateur() {
		super();		
	}	
	

}
