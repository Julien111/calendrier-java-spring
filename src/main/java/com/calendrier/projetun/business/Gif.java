package com.calendrier.projetun.business;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name="gif")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public abstract class Gif {
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	protected Long id;
		
    @Column(name = "date_heure_ajout")
	protected LocalDateTime dateHeureAjout;
	
	@Column(name = "legende")
	protected String legende;
	
	@ManyToOne
	private Utilisateur utilisateur;
	
	@OneToMany(mappedBy="gif", fetch=FetchType.EAGER)
	private List<Reaction> reactions;
	
	@OneToOne(mappedBy="gif", fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, orphanRemoval = true)
    private Jour jour;
	
	protected Gif() {		
		super();
	}	
	
}
