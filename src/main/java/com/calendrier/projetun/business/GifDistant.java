package com.calendrier.projetun.business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name="gifdistant")
public class GifDistant extends Gif {
	
	@Column(name = "url")
	private String url;

	public GifDistant() {
		super();		
	}	
	
}
