package com.calendrier.projetun.business;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@Table(name="reaction")
public class Reaction {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
    @Column(name = "date_heure")
	private LocalDateTime dateHeure;
	
	@ManyToOne
	private Gif gif;
	
	@ManyToOne
	@JoinColumn(name="id_utilisateur")
	Utilisateur utilisateur;	
	
	@ManyToOne
	@JoinColumn(name="id_emotion")
	Emotion emotion;
	
	public Reaction() {
		super();		
	}	
	
	
	
}
