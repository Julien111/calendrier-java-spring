package com.calendrier.projetun.business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name="gifteleverse")
public class GifTeleverse extends Gif {
	
	@Column(name="nom_fichier_original")
	private String nomFichierOriginal;

	public GifTeleverse() {
		super();		
	}	

}
