package com.calendrier.projetun.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.calendrier.projetun.business.GifTeleverse;


public interface GifTeleverseDao extends JpaRepository<GifTeleverse, Long> {

}
