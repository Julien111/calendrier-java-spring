package com.calendrier.projetun.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.calendrier.projetun.business.Theme;


public interface ThemeDao extends JpaRepository<Theme, Long> {

	// Requête par dérivation
	Theme findByNom(String nom);	
	
	@Query("FROM Theme WHERE nom=:nom")
	Theme findByNomHQL(@Param("nom") String nom);

	/**
	 * 
	 * @param nom
	 * @return
	 */
	@Query(value="SELECT * FROM theme WHERE nom LIKE '%nom%'", nativeQuery = true)
	Theme findByNomSQL(@Param("nom") String nom);

	@Query("SELECT DISTINCT theme FROM Utilisateur")
	List<Theme> findByTheme();
}
