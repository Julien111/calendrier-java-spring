package com.calendrier.projetun.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.calendrier.projetun.business.Emotion;


public interface EmotionDao extends JpaRepository<Emotion, Long> {

	// Requête par dérivation ("Query method"): il n'y a pas d'annotation @Query, par conséquent
	// Spring Data essaie de construire une requête HQL en analysant le nom 
	// de la méthode Java	
	/**
	 * Méthode qui renvoie l'émotion dont le nom est donné en paramètre
	 * 
	 * @param nom de l'emotion
	 * @return un objet de type Emotion
	 */
	Emotion findByNom(String nom);
	
	Emotion findByCode(String leCodeHTML);
	
	List<Emotion> findFirst2ByNomContainingIgnoreCase(String nom);
	
	List<Emotion> findByNomStartingWith(String debutDuNomRecherche);
	
	// L'annotation @Query accueille par défaut une requête HQL
	// utilisation de la fonction lower
	@Query(value="FROM Emotion WHERE lower(nom) LIKE 's%'", nativeQuery = false)
	List<Emotion> findEmotionsHavingNameStartingWithS();

	// Double sécurité
	@Modifying // Sans cette annotation : Not supported for DML operations [UPDATE fr.sparks.fx.calendrier_gif.business.Emotion SET nom=upper(nom)]
	@Transactional // Sans cette annotation : Caused by: javax.persistence.TransactionRequiredException: Executing an update/delete query
	@Query("UPDATE Emotion SET nom=upper(nom)")
	void updateNom();
	
	/**
	 * Supprime les émotions dont le nom débute par le paramètre debut
	 * 
	 * @param debut
	 * @return
	 */
	long deleteByNomStartingWith(String debut);
	
	long countByNomStartingWith(String debut);
	
	boolean existsByNom(String nom);
	
	
	
}