package com.calendrier.projetun.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.calendrier.projetun.business.Emotion;
import com.calendrier.projetun.business.Gif;
import com.calendrier.projetun.business.Jour;
import com.calendrier.projetun.business.Reaction;
import com.calendrier.projetun.business.Utilisateur;


public interface ReactionDao extends JpaRepository<Reaction, Long> {

	/**
	 * Méthode qui renvoie la liste des toutes les réactions du Gif donné en paramètre
	 * @param gif
	 * @return
	 */
	List<Reaction> findByGif(Gif gif);

	Reaction findLastByGifAndUtilisateurAndEmotion(Gif gif, Utilisateur utilisateur, Emotion emotion);

	// Spring Data va interpréter le nom de la méthode et le traduire en HQL
	List<Reaction> findByDateHeureBetween(LocalDateTime dateDebut, LocalDateTime dateFin);

	@Query("""
			SELECT r
			FROM Reaction r
			WHERE r.id = :id and r.gif.jour.date >= :dateDebut and r.gif.jour.date < :dateFin and r.utilisateur.email like :email
			""")
	List<Reaction> findFirstByIdAndDateAndUtilisateurEmailLike(@Param("id") Long idReaction,
			@Param("dateDebut") LocalDateTime dateDebut, 
			@Param("dateFin") LocalDateTime dateFin,
			@Param("email") String email);
	
	List<Reaction> findLast5ByGif(Gif gif);
	
	List<Reaction> findByGifJour(Jour jour);
	
}