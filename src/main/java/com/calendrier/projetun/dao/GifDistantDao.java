package com.calendrier.projetun.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.calendrier.projetun.business.GifDistant;

public interface GifDistantDao extends JpaRepository<GifDistant, Long> {

	long deleteByUrlLike(String source);
}
