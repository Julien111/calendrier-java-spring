package com.calendrier.projetun.dto;

import java.time.LocalDateTime;

import com.calendrier.projetun.business.Theme;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UtilisateurDto {	
			
	String nom;	
	
	String prenom;	
	
	String email;	
	
	String motDePasse;		
    
	LocalDateTime dateHeureInscription;	
	
	Theme theme;	

	public UtilisateurDto(String nom, String prenom, String email, String motDePasse,
			LocalDateTime dateHeureInscription, Theme theme) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.motDePasse = motDePasse;
		this.dateHeureInscription = dateHeureInscription;
		this.theme = theme;
	}	

}
