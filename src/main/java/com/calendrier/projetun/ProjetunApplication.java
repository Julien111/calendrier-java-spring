package com.calendrier.projetun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetunApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetunApplication.class, args);
	}

}
